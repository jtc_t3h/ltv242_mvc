package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.EmployeeDAO;
import vn.t3h.chapter4.dao.EmployeeDAOImpl;
import vn.t3h.chapter4.domain.EmployeeEntity;

/**
 * Servlet implementation class EmployeeServlet
 */
@WebServlet("/chapter4/employee.html")
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private EmployeeDAO employeeDAO;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeServlet() {
        employeeDAO = new EmployeeDAOImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<EmployeeEntity> listOfEmployee = null;
		try {
			listOfEmployee = employeeDAO.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("listOfEmployee", listOfEmployee);
		request.getRequestDispatcher("../pages/chapter4/employee.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
