package vn.t3h.chapter4.domain;

import java.io.Serializable;

public class DepartmentEntity implements Serializable{

	private Long id;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
