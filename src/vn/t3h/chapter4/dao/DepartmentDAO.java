package vn.t3h.chapter4.dao;

import java.util.List;

import vn.t3h.chapter4.domain.DepartmentEntity;


public interface DepartmentDAO {

public List<DepartmentEntity>	findAll();
    
    public int insert(DepartmentEntity e);
    public int update(DepartmentEntity e);
    
    public int delete(DepartmentEntity e);
    public int delete(List<DepartmentEntity> employees);
}
