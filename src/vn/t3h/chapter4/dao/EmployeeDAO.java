package vn.t3h.chapter4.dao;

import java.sql.SQLException;
import java.util.List;

import vn.t3h.chapter4.domain.EmployeeEntity;


public interface EmployeeDAO {

    public List<EmployeeEntity>	findAll() throws SQLException;
    
    public int insert(EmployeeEntity e);
    public int update(EmployeeEntity e);
    
    public int delete(EmployeeEntity e);
    public int delete(List<EmployeeEntity> employees);
}
