package vn.t3h.chapter4.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vn.t3h.chapter4.domain.EmployeeEntity;


public class EmployeeDAOImpl implements EmployeeDAO {

	@Override
	public List<EmployeeEntity> findAll() throws SQLException {
		List<EmployeeEntity> listOfEmployee = new ArrayList<>();
		
//		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
//		String username = "root";
//		String password = "";
//
//		Connection con = null;
//		Statement statement = null;
//		ResultSet resultSet = null;
//		try {
//			con = DriverManager.getConnection(url, username, password);
//			statement = con.createStatement();
//			String sqlSelect = "select * from employee";
//			resultSet = statement.executeQuery(sqlSelect);
//
//			while (resultSet.next()) {
//				EmployeeEntity employeeEntity = new EmployeeEntity();
//				employeeEntity.setId(resultSet.getLong(1));
//				employeeEntity.setName(resultSet.getString(2));
//				
//				listOfEmployee.add(employeeEntity);
//			}
//		} catch (SQLException e) {
//			System.out.println("Connect unsuccessful.");
//			e.printStackTrace();
//		} finally {
//			resultSet.close();
//			statement.close();
//			con.close();
//		}
		
		listOfEmployee.add(new EmployeeEntity(1L, "Nguyen Van A"));
		listOfEmployee.add(new EmployeeEntity(2L, "Nguyen Van B"));

		return listOfEmployee;
	}

	@Override
	public int insert(EmployeeEntity e) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(EmployeeEntity e) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(EmployeeEntity e) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(List<EmployeeEntity> employees) {
		// TODO Auto-generated method stub
		return 0;
	}

}
