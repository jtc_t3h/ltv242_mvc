package vn.t3h.chapter5;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("Servlet context destroy.");
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("Servlet context initialize.");
	}

	
}
