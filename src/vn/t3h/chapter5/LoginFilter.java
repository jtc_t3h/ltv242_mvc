package vn.t3h.chapter5;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		HttpSession session = req.getSession(false);
		System.out.println(req.getRequestURI());
		if (req.getRequestURI().endsWith("login.jsp") || req.getRequestURI().endsWith("LoginServlet")
				|| req.getRequestURI().endsWith(".css")
				|| (session != null && session.getAttribute("username") != null)) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect(req.getContextPath() + "/pages/chapter2/login.jsp");
		}
	}

}
