package vn.t3h.chapter6.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.transaction.jta.platform.internal.SynchronizationRegistryBasedSynchronizationStrategy;

import vn.t3h.chapter6.dao.DepartmentDAO;
import vn.t3h.chapter6.entity.Department;

/**
 * Servlet implementation class DepartmentServlet
 */
@WebServlet("/chapter6/department.html")
public class DepartmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private DepartmentDAO departmentDAO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DepartmentServlet() {
        departmentDAO = new DepartmentDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Department> listOfDepartment = departmentDAO.list2();
		listOfDepartment.forEach(department -> System.out.println(department));
		
		request.setAttribute("listOfDepartment", listOfDepartment);
		request.getRequestDispatcher("/pages/chapter6/department-list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
