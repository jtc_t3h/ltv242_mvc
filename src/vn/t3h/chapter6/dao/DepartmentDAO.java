package vn.t3h.chapter6.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import vn.t3h.chapter6.entity.Department;
import vn.t3h.chapter6.utils.HibernateUtils;

public class DepartmentDAO {

	public List<Department> list(){
		try (Session session = HibernateUtils.getSessionFactory().openSession()){
		 	Query query = session.createQuery("from Department");
		 	return query.list();
		}
	}
	
	public List<Department> list2(){
		try (Session session = HibernateUtils.getSessionFactory().openSession()){
			Query query = session.createNativeQuery("select * from don_vi", Department.class);
			return query.list();
		}
	}
}
