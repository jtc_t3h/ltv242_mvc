package vn.t3h.chapter7;

import java.io.PrintWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

public class IfTagHandler implements Tag{

	private boolean test;
	private PageContext pageContext;
	private Tag parentTag;
	
	public void setTest(boolean test) {
		this.test = test;
	}
	
	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		// do Something
		if (test) {
			return EVAL_BODY_INCLUDE;
		}
		return SKIP_BODY;
	}

	@Override
	public Tag getParent() {
		return parentTag;
	}

	@Override
	public void release() {
		
	}

	@Override
	public void setPageContext(PageContext pc) {
		this.pageContext = pc;
	}

	@Override
	public void setParent(Tag t) {
		this.parentTag = t;
	}

}
