package vn.t3h.chapter7;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.IterationTag;
import javax.servlet.jsp.tagext.Tag;

public class LoopTagHandler implements IterationTag {

	private PageContext pageContext;
	private Tag parentTag;
	
	private int counter;
	
	public void setCounter(int counter) {
		this.counter = counter;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public Tag getParent() {
		return parentTag;
	}

	@Override
	public void release() {
	}

	@Override
	public void setPageContext(PageContext pc) {
		this.pageContext = pc;
	}

	@Override
	public void setParent(Tag t) {
		this.parentTag = t;
	}

	@Override
	public int doAfterBody() throws JspException {
		while (--counter > 0) {
			return EVAL_BODY_AGAIN;
		}
		return SKIP_BODY;
	}

}
