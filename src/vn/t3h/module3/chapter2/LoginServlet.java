package vn.t3h.module3.chapter2;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/chapter2/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		// neu nguoi dung nhap cung username va password thi dang nhap thanh cong, nguoc lai -> fail
		if (!username.isEmpty() && !password.isEmpty() && username.equals(password)) {
			System.out.println("Login successful!");
//			request.setAttribute("username", username);
			
			HttpSession session = request.getSession();
			session.setAttribute("username", username);
			
			response.sendRedirect("../pages/chapter2/welcome.jsp");
		} else {
			System.out.println("Login fail!");
			request.setAttribute("messageError", "Username hoac Password khong dung.");
			
			request.getRequestDispatcher("../pages/chapter2/login.jsp").forward(request, response);
		}
	}

}
