package vn.t3h.module3.chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Bai2_SumTwoNumberServlet
 */
@WebServlet("/chapter2/sum.html")
public class Bai2_SumTwoNumberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_SumTwoNumberServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		
		if (request.getParameter("a") != null && request.getParameter("b") != null) {
			doPost(request, response);
		}
		
		try (PrintWriter pw = response.getWriter()) {
			pw.write("<form method=\"get\">");
			pw.write("<p>");
			pw.write("<label>Number a</label>");
			pw.write("<input type=\"number\" name=\"a\">");
			pw.write("</p>");
			pw.write("<p>");
			pw.write("<label>Number b</label>");
			pw.write("<input type=\"number\" name=\"b\">");
			pw.write("</p>");
			pw.write("<p>");
			pw.write("<button>Sum</button>");
			pw.write("</p>");
			pw.write("</form>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int a = Integer.parseInt(request.getParameter("a"));
		int b = Integer.parseInt(request.getParameter("b"));
		try (PrintWriter pw = response.getWriter()) {
			pw.printf("<p>Sum %d + %d = %d<p>", a, b, a + b);
		}
	}

}
