package vn.t3h.module3.chapter2;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LifeCycleCallbackMethod
 */
@WebServlet("/chapter2/life-cycle-callback-method")
public class LifeCycleCallbackMethod extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LifeCycleCallbackMethod() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Life cycle callback method: service.");
		
		ServletContext context = getServletContext();
		// duyet cac context-param
		Enumeration<String> paraNames = context.getInitParameterNames();
		while( paraNames.hasMoreElements()) {
			String paraName = paraNames.nextElement();
			String paraValue = context.getInitParameter(paraName);
			System.out.println(paraName + " = " + paraValue);
		}
	}

	@Override
	public void init() throws ServletException {
		System.out.println("Life cycle callback method: init.");
	}
	
	public void destroy() {
		System.out.println("Life cycle callback method: destroy");
	}

}
