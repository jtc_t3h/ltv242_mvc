package vn.t3h.module3.chapter2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.management.snmp.jvmmib.EnumJvmMemManagerState;

public class HelloServletByXML extends HttpServlet{
	private ServletConfig config;
	
	@Override
	public void init(ServletConfig config) {
		System.out.println("HelloServletByXML: init method()");
		this.config = config;
		
		// Duyet tat ca cac init-param
		Enumeration<String> paramNames = config.getInitParameterNames();
		while (paramNames.hasMoreElements()) {
			String paraName = (String) paramNames.nextElement();
			String paraValue = config.getInitParameter(paraName);
			System.out.println(paraName + " = " + paraValue);
		}
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter pw = resp.getWriter();
		pw.println("<html>");
		pw.println("<body><h1>Welcome Java EE.</h1></body>");
		pw.println("</html>");
	}

	
}
