package vn.t3h.module3.chapter3;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ELServlet
 */
@WebServlet("/chapter3/el.html")
public class ELServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ELServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("messageResponse", "Hello, messageResponse!");
		
		Employee employee = new Employee();
		employee.setName("De Pham");
		request.setAttribute("employee", employee);
		
		request.getRequestDispatcher("../pages/chapter3/el.jsp").forward(request, response);
	}

}
