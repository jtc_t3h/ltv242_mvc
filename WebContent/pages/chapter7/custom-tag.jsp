<%@ taglib prefix="test" uri="/WEB-INF/t3h.tld" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <test:if test="true">
     <h1>Hello</h1>
  </test:if>
  
  <test:loop counter="5">
    <h1>World</h1>
  </test:loop>
</body>
</html>