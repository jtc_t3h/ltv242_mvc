<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="home.title"/> </title>
<link rel="stylesheet" type="text/css" href="<c:url value='/css/css.css'/> ">
</head>
<body>
	<div class="title">
		<div class="container">
			<div class="brand">
				<a href="/MVC">Module 3</a>
			</div>
		</div>
	</div>
	<div class="sidebar">
		<ul>
			<li><a href="helloworld.html">Hello World</a></li>
			<li><a href="<c:url value='/chapter2/sum2.html'/>" >Sum Two Numbers</a></li>
			<li><a href="summultinumbers.html">Sum Multiple Numbers</a></li>
			<li><a href="register.jsp">Register</a></li>
			<li><a href="login.jsp">Login</a></li>
			<li><a href="multiplicationtable.jsp">Multiplication Table</a></li>
			<li><a href="upload.html">Upload Image</a></li>
			<li><a href="upload/multi.html">Multiple Upload Image</a></li>
			<li><a href="template.jsp">Template Example</a></li>
			<li><a href="admin/publisher.html">Publisher</a>
			<li><a href="admin/category.html">Category</a></li>
			<li><a href="home.html">Home</a></li>
			<li><a href="auth/register.html">Register</a></li>
			<li><a href="auth/logon.html">Log On</a></li>
			<li><a href="admin/invoice.html">Invoice</a></li>
		</ul>
	</div>
	<div class="main">
      <h1>Ket qua: ${result }</h1>	
	</div>
</body>
</html>