<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <h1>DANH SACH EMPLOYEE</h1>
  <table>
     <thead>
       <td>ID</td>
       <td>Name</td>
     </thead>
     <tbody>
       <c:forEach items="${listOfEmployee }" var="employee">
       <tr>
         <td>${employee.id }</td>
         <td>${employee.name }</td>
       </tr>
       </c:forEach>
     </tbody>
  </table>
</body>
</html>