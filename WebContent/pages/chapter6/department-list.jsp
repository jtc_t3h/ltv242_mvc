<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <table>
    <tr>
      <td>ID</td>
      <td>Ten</td>
    </tr>
    <c:forEach var="department" items="${listOfDepartment }">
     <tr>
      <td>${department.id }</td>
      <td>${department.name }</td>
    </tr>
    </c:forEach>
  </table>
</body>
</html>