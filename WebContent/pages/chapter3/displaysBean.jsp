<jsp:useBean id="employee"  class="vn.t3h.module3.chapter3.Employee" scope="session"></jsp:useBean>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <jsp:setProperty property="*" name="employee"/>
  
  <%-- <h1>id:<jsp:getProperty property="id" name="employee"/></h1>
  <h1>name:<jsp:getProperty property="name" name="employee"/></h1> --%>
  
  <jsp:forward page="resultBean.jsp"></jsp:forward>
</body>
</html>