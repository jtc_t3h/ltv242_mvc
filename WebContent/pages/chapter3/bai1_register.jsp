<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Module 03 LẬP TRÌNH WEB VỚI JAVA</title>
<!-- <link rel="stylesheet" type="text/css" href="/LTV242_MVC/css/css.css">  -->
<link rel="stylesheet" type="text/css" href="../../css/css.css">
</head>
<body>
	<div class="title">
		<div class="container">
			<div class="brand">
				<a href="/MVC">Module 3</a>
			</div>
		</div>
	</div>
	<div class="sidebar">
		<ul>
			<li><a href="helloworld.html">Hello World</a></li>
			<li><a href="../../chapter2/sum.html">Sum Two Numbers</a></li>
			<li><a href="summultinumbers.html">Sum Multiple Numbers</a></li>
			<li><a href="register.jsp">Register</a></li>
			<li><a href="login.jsp">Login</a></li>
			<li><a href="multiplicationtable.jsp">Multiplication Table</a></li>
			<li><a href="upload.html">Upload Image</a></li>
			<li><a href="upload/multi.html">Multiple Upload Image</a></li>
			<li><a href="template.jsp">Template Example</a></li>
			<li><a href="admin/publisher.html">Publisher</a>
			<li><a href="admin/category.html">Category</a></li>
			<li><a href="home.html">Home</a></li>
			<li><a href="auth/register.html">Register</a></li>
			<li><a href="auth/logon.html">Log On</a></li>
			<li><a href="admin/invoice.html">Invoice</a></li>
		</ul>
	</div>
	<div class="main">
		<form method="post" class="form">
			<p>
				<label>Username</label> <input type="text" name="usr">
			</p>
			<p>
				<label>Password</label> <input type="password" name="pwd">
			</p>
			<p>
				<label>Email</label> <input type="text" name="email">
			</p>
			<p>
				<label>Gender</label> <select name="gender">
					<option value="Male">Male</option>
					<option value="Female">Female</option>
					<option value="Undefined">Undefined</option>
				</select>
			</p>
			<p>
				<button>Register</button>
			</p>
		</form>
		<%
			if (request.getMethod().equalsIgnoreCase("post")) {
		%>
		<p>	Username:<%=request.getParameter("usr")%></p>
		<p>	Password:<%=request.getParameter("pwd")%></p>
		<p>	Email:<%=request.getParameter("email")%></p>
		<p>	Gender:	<%=request.getParameter("gender")%></p>
		<%
			}
		%>
	</div>
</body>
</html>